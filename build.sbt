import java.util.jar.Attributes.Name._

import NativePackagerHelper._

val title = "P4: Recommender engine"

name := "add-project"
organization := "pl.edu.pjatk.s10888"
maintainer := "s10888@pjwstk.edu.pl"

version := "1.0.0"

scalaVersion := "2.11.12"

val excludeLog4j = Seq()

libraryDependencies := Seq(
  "org.apache.spark" %% "spark-sql" % "2.4.3" excludeAll(ExclusionRule(organization = "log4j"), ExclusionRule(organization = "org.slf4j", name = "slf4j-log4j12")),
  "org.apache.spark" %% "spark-mllib" % "2.4.3" excludeAll(ExclusionRule(organization = "log4j"), ExclusionRule(organization = "org.slf4j", name = "slf4j-log4j12")),
  "org.slf4j" % "log4j-over-slf4j" % "1.7.25",
  "com.google.code.gson" % "gson" % "2.8.2",
  "org.springframework.boot" % "spring-boot-starter" % "2.1.5.RELEASE",
  "org.springframework.shell" % "spring-shell-starter" % "2.0.0.RELEASE"
)

mainClass := Some("pl.edu.pjatk.s10888.Main")
mappings in Universal ++= directory("data")

packageOptions in(Compile, packageBin) +=
  Package.ManifestAttributes(
    IMPLEMENTATION_TITLE -> title,
    SPECIFICATION_TITLE -> title
  )

enablePlugins(JavaAppPackaging)

