package pl.edu.pjatk.s10888.freqsets

import java.io.FileNotFoundException
import java.nio.file.{Files, Paths}

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.springframework.stereotype.Service
import pl.edu.pjatk.s10888.freqsets.model.{DataSource, TopProduct}

@Service
class DataSourceLoader {

  def loadDefault()(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._

    val possibleDs = List(
      "./data/retail.dat.txt",
      "../data/retail.dat.txt"
    ).find(p => Files.exists(Paths.get(p)))
    spark.sparkContext.textFile(possibleDs.getOrElse(
      throw new FileNotFoundException("Unable to locate dataset file in any of the known locations." +
        " Make sure, that your current path matches the instalation path")
    )).toDS()
      .map(t => t.split(" "))
      .toDF("items").coalesce(4)
  }

  def loadDefaultWithStatistics()(implicit spark: SparkSession): DataSource = {
    import spark.implicits._
    val dataSource = loadDefault().cache()
    val txCount = dataSource.count()
    val products = dataSource.select(explode(col("items")).as("product"))
    val productCount = products.distinct().count()
    val topProducts = products
      .groupBy(col("product"))
      .agg(count("product").as("product_count"))
      .orderBy(desc("product_count"))
      .limit(10)
      .map(r => TopProduct(r.getString(0), r.getLong(1)))
      .collect()
      .toList
    DataSource(dataSource, txCount, productCount, topProducts)
  }

}
