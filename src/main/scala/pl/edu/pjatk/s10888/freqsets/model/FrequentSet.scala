package pl.edu.pjatk.s10888.freqsets.model

import org.apache.spark.sql.Row

case class FrequentSet(items: Seq[String], frequency: Long, support: Double)

object FrequentSet {
  def apply(row: Row): FrequentSet = {
    new FrequentSet(row.getSeq[String](0), row.getLong(1), row.getDouble(2))
  }
}
