package pl.edu.pjatk.s10888.freqsets.model

import org.apache.spark.sql.Row

case class Predictions(item: String, predictions: Seq[String])

object Predictions {
  def apply(row: Row): Predictions = new Predictions(row.getSeq(0).head, row.getSeq(1))
}
