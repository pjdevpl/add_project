package pl.edu.pjatk.s10888.freqsets.model

case class TopProduct(productId: String, count: Long)
