package pl.edu.pjatk.s10888.freqsets.model

import org.apache.spark.sql.Row

case class AssociationRule(antecedent: Seq[String], consequent: Seq[String], confidence: Double, lift: Double)

object AssociationRule {
  def apply(r: Row): AssociationRule = AssociationRule(r.getSeq[String](0), r.getSeq[String](1), r.getDouble(2), r.getDouble(3))
}
