package pl.edu.pjatk.s10888.freqsets.model

import org.apache.spark.sql.DataFrame

case class DataSource(df: DataFrame, txCount: Long, productsCount: Long, topProducts: List[TopProduct])
