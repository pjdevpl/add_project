package pl.edu.pjatk.s10888.freqsets

import org.apache.spark.ml.fpm.{FPGrowth, FPGrowthModel}
import org.apache.spark.sql.{Dataset, SparkSession}
import org.springframework.beans.factory.annotation.Autowired
import pl.edu.pjatk.s10888.freqsets.model.{AssociationRule, Predictions}
import org.apache.spark.sql.functions._
import org.springframework.stereotype.Service

@Service
class AssociationRulesCalculator(@Autowired private val dataSourceLoader: DataSourceLoader) {

  def calculateAssociationRules(minSupport: Double, minConfidence: Double, minSize: Int, maxSize: Int)
                               (implicit spark: SparkSession): Dataset[AssociationRule] = {
    import spark.implicits._

    val model = prepareModel(minSupport, minConfidence)

    val rules = model.associationRules
    rules.map(AssociationRule.apply).filter { a =>
      val ruleSize = a.antecedent.size
      ruleSize >= minSize && ruleSize <= maxSize
    }.orderBy(desc("confidence"))
  }

  def collectAssociationRules(minSupport: Double, minConfidence: Double, minSize: Int, maxSize: Int, limit: Int)
                             (implicit sparkSession: SparkSession): List[AssociationRule] = {
    val rules = calculateAssociationRules(minSupport, minConfidence, minSize, maxSize)
    rules.limit(limit).collect().toList
  }

  def findAssociationRulesFor(productId: String, minSupport: Double, minConfidence: Double, limit: Int)
                             (implicit sparkSession: SparkSession): List[AssociationRule] = {
    val rules = calculateAssociationRules(minSupport, minConfidence, 1, 1)
    rules.filter(_.antecedent == Seq(productId)).limit(limit).collect().toList
  }

  def generateRecommendations(productId: String, minSupport: Double, minConfidence: Double)(implicit spark: SparkSession): Predictions = {
    import spark.implicits._

    val model = prepareModel(minSupport, minConfidence)
    model.transform(List(Seq(productId)).toDF("items")).map(Predictions.apply).first()
  }

  private def prepareModel(minSupport: Double, minConfidence: Double)(implicit spark: SparkSession) = {
    val dataset = dataSourceLoader.loadDefault().cache()

    val fpgrowth = new FPGrowth().setItemsCol("items").setMinSupport(minSupport).setMinConfidence(minConfidence).setNumPartitions(4)
    fpgrowth.fit(dataset)
  }

}
