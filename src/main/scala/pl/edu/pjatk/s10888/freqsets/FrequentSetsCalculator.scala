package pl.edu.pjatk.s10888.freqsets

import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions._
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pl.edu.pjatk.s10888.freqsets.model.FrequentSet
import org.apache.spark.ml.fpm.FPGrowth

import scala.util.Try

@Service
class FrequentSetsCalculator(@Autowired private val dataSourceLoader: DataSourceLoader) {

  def calculateFrequentSets(minSupport: Double, minSize: Int)(implicit spark: SparkSession): Dataset[FrequentSet] = {
    import spark.implicits._

    val dataset = dataSourceLoader.loadDefault().cache()

    val datasetSize = dataset.count()

    val fpgrowth = new FPGrowth().setItemsCol("items").setMinSupport(minSupport).setNumPartitions(4)
    val model = fpgrowth.fit(dataset)

    model.freqItemsets
      .filter(size(col("items")) >= lit(minSize))
      .withColumn("support", col("freq") / lit(datasetSize))
      .orderBy(desc("support"))
      .map(FrequentSet(_))
  }

  def findFrequentlyBoughtWith(productId: String, minSupport: Double)(implicit spark: SparkSession): List[String] = {
    import spark.implicits._
    val frequentSets = calculateFrequentSets(minSupport, 2)
    Try(frequentSets.filter(_.items.contains(productId)).map(_.items).reduce {(a, b) => a ++ b}
      .distinct
      .filter(_ != productId)
      .sortBy(_.toInt).toList).getOrElse(List())
  }

  def collectFrequentSets(minSupport: Double, minSize: Int, limit: Int)(implicit spark: SparkSession): List[FrequentSet] = {
    val frequentSets = calculateFrequentSets(minSupport, minSize)
    frequentSets.limit(limit)
      .collect()
      .toList
  }

}
