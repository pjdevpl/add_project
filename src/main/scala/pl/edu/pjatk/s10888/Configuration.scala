package pl.edu.pjatk.s10888

import org.apache.spark.sql.SparkSession
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.{Configuration => SpringConfiguration}

@SpringConfiguration
class Configuration {
  @Bean
  def sparkSession(): SparkSession = SparkSession
    .builder
    .master("local[4]")
    .appName("Recomender engine for products")
    .config("spark.ui.enabled", "true")
    .config("spark.executor.memory", "2g")
    .config("spark.driver.memory", "2g")
    .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .getOrCreate()
}
