package pl.edu.pjatk.s10888.utils

import org.springframework.context.ApplicationContext

object ContextHolder {
  var ctx: ApplicationContext = _
}
