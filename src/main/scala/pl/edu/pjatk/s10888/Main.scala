package pl.edu.pjatk.s10888

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.{ApplicationContextInitializer, ConfigurableApplicationContext}
import pl.edu.pjatk.s10888.utils.ContextHolder


object Main {
  def main(args: Array[String]): Unit = {
    val app = new SpringApplication(classOf[Main])
    app.addInitializers(new ApplicationContextInitializer[ConfigurableApplicationContext] {
      override def initialize(applicationContext: ConfigurableApplicationContext): Unit = {
        ContextHolder.ctx = applicationContext
      }
    })
    app.run(args: _*)
  }
}

@SpringBootApplication(scanBasePackages = Array("pl.edu.pjatk.s10888"))
@ComponentScan
class Main



