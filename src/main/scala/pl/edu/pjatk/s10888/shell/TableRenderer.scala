package pl.edu.pjatk.s10888.shell

import org.springframework.shell.table.CellMatchers.table
import org.springframework.shell.table.{ArrayTableModel, BorderStyle, NoWrapSizeConstraints, SimpleHorizontalAligner, Table, TableBuilder}
import pl.edu.pjatk.s10888.freqsets.model.{AssociationRule, FrequentSet, Predictions, TopProduct}

import scala.language.implicitConversions

object TableRenderer {

  private[shell] def renderTable(header: List[String], data: List[List[String]]): Table = {
    val alignedHeader = header.map(h => s" $h")
    val tableContent = data.foldLeft(List(Array(alignedHeader: _*))) { (buf, p) => buf ++ List(Array(p: _*)) }
      .map(_.map(_.asInstanceOf[AnyRef])).toArray
    val model = new ArrayTableModel(tableContent)
    val tableBuilder = new TableBuilder(model)
    tableBuilder.on(table()).addAligner(SimpleHorizontalAligner.right).addSizer(new NoWrapSizeConstraints)
    tableBuilder.addHeaderAndVerticalsBorders(BorderStyle.fancy_light_double_dash).build
  }

  private[shell] def render(collection: Seq[String]): String = collection.mkString("(", ", ", ")")

  private[shell] def render(d: Double): String = BigDecimal(d).setScale(10, BigDecimal.RoundingMode.HALF_UP).toString()
}

object ModelTableRenderer {
  import TableRenderer._

  implicit private[shell] def renderTopProducts(frequentSets: Seq[TopProduct]): List[List[String]] = {
    frequentSets.map(s => List(s.productId, s.count.toString)).toList
  }

  implicit private[shell] def renderFrequentSets(frequentSets: Seq[FrequentSet]): List[List[String]] = {
    frequentSets.map(s => List(render(s.items), s.frequency.toString, render(s.support))).toList
  }

  implicit private[shell] def renderAssociationRules(associationRules: Seq[AssociationRule]): List[List[String]] = {
    associationRules.map(r => List(render(r.antecedent), render(r.consequent), render(r.confidence), render(r.lift))).toList
  }
}
