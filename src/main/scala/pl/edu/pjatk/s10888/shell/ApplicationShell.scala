package pl.edu.pjatk.s10888.shell

import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory
import org.springframework.shell.standard.{ShellComponent, ShellMethod, ShellOption}
import org.springframework.shell.table.Table
import pl.edu.pjatk.s10888.freqsets.{AssociationRulesCalculator, DataSourceLoader, FrequentSetsCalculator}
import pl.edu.pjatk.s10888.shell.TableRenderer._
import pl.edu.pjatk.s10888.shell.ModelTableRenderer._
import pl.edu.pjatk.s10888.utils.ContextHolder._

@ShellComponent
class ApplicationShell {

  private val log = LoggerFactory.getLogger(classOf[ApplicationShell])

  @ShellMethod("Show data source statistics")
  def dsStatistics(): Table = {
    implicit val spark = sparkSession
    val loader = ctx.getBean(classOf[DataSourceLoader])
    log.info("Gathering data source statistics...")
    val ds = loader.loadDefaultWithStatistics()
    log.info(s"Transactions count: ${ds.txCount}")
    log.info(s"Products count: ${ds.productsCount}")
    log.info("Top products: ")

    renderTable(List("Product ID", "No. of items sold"), ds.topProducts)
  }

  @ShellMethod("Compute frequent sets using FPGrowth")
  def frequentSets(@ShellOption(defaultValue = "0.01") minSupport: Double,
                   @ShellOption(defaultValue = "1") minSize: Int,
                   @ShellOption(defaultValue = "20") limit: Int): Table = {
    implicit val spark = sparkSession

    val calculator = ctx.getBean(classOf[FrequentSetsCalculator])
    log.info("Calculating frequent sets...")
    val frequentSets = calculator.collectFrequentSets(minSupport, minSize, limit)

    renderTable(List("Products", "Frequency", "Support"), frequentSets)
  }

  @ShellMethod("Find products frequently bought with a specified one")
  def frequentlyBoughtWith(productId: String,
                           @ShellOption(defaultValue = "0.01") minSupport: Double): Unit = {
    implicit val spark = sparkSession
    val calculator = ctx.getBean(classOf[FrequentSetsCalculator])
    log.info(s"Finding products frequently bought with $productId...")

    val frequentlyBought = calculator.findFrequentlyBoughtWith(productId, minSupport)
    log.info(s"Found products: ${render(frequentlyBought)}")
  }

  @ShellMethod("Calculate association rules")
  def associationRules(@ShellOption(defaultValue = "0.01") minSupport: Double,
                       @ShellOption(defaultValue = "0.01") minConfidence: Double,
                       @ShellOption(defaultValue = "0") minSize: Int,
                       @ShellOption(defaultValue = "2147483647") maxSize: Int,
                       @ShellOption(defaultValue = "20") limit: Int): Table = {
    implicit val spark = sparkSession
    val calculator = ctx.getBean(classOf[AssociationRulesCalculator])
    log.info("Calculating association rules...")

    val rules = calculator.collectAssociationRules(minSupport, minConfidence, minSize, maxSize, limit)
    renderTable(List("Antecedent", "Consequent", "Confidence", "Lift"), rules)
  }

  @ShellMethod("Calculate association rules of type a->b for a given product a")
  def associationRulesFor(productId: String,
                          @ShellOption(defaultValue = "0.01") minSupport: Double,
                          @ShellOption(defaultValue = "0.01") minConfidence: Double,
                          @ShellOption(defaultValue = "20") limit: Int): Table = {

    implicit val spark = sparkSession
    val calculator = ctx.getBean(classOf[AssociationRulesCalculator])
    log.info("Calculating association rules...")

    val rules = calculator.findAssociationRulesFor(productId, minSupport, minConfidence, limit)
    renderTable(List("Antecedent", "Consequent", "Confidence", "Lift"), rules)
  }

  @ShellMethod("Recommend products, based on a selected one")
  def recommend(productId: String,
                @ShellOption(defaultValue = "0.01") minSupport: Double,
                @ShellOption(defaultValue = "0.01") minConfidence: Double): Unit = {

    implicit val spark = sparkSession
    val calculator = ctx.getBean(classOf[AssociationRulesCalculator])
    log.info(s"Calculating recommendations for product $productId")

    val rules = calculator.generateRecommendations(productId, minSupport, minConfidence)
    log.info(s"Found recommendations: ${render(rules.predictions)}")
  }

  private def sparkSession: SparkSession = ctx.getBean("sparkSession", classOf[SparkSession])

}
